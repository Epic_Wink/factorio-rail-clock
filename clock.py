#!/usr/bin/env python3
import datetime as dt
import shutil
import time
import pathlib
import tempfile

import delegator

import clock_config

server = None
client = None
script_dir = pathlib.Path(__file__).parent


def generate_time_commands(time_):
    hour = time_.hour
    minute = time_.minute

    hour_two_digits = str(hour).zfill(2)
    min_two_digits = str(minute).zfill(2)

    commands = """
        /silent-command remote.call("clock", "set", {hr_1}, {hr_2}, {min_1}, {min_2},
        {{hr_1=global.hr_1, hr_2=global.hr_2, min_1=global.min_1, min_2=global.min_2, reset=global.reset}})
    """.format(hr_1=hour_two_digits[0],
               hr_2=hour_two_digits[1],
               min_1=min_two_digits[0],
               min_2=min_two_digits[1])

    return commands


def prepare_run_directory(path, include_server_config):
    data_path = clock_config.data_path
    base_config = script_dir / "config.ini"
    save_path = script_dir / "trainclock.zip"
    mods_path = script_dir / "mods"
    base_server_settings = script_dir / "server-settings.json"

    server_folder = path

    if server_folder.exists():
        shutil.rmtree(server_folder)
    server_folder.mkdir(parents=True)

    use_config = server_folder / "config.ini"
    use_write = server_folder / "write"
    use_server_config = server_folder / "server-settings.json"
    saves = use_write / "saves"
    mods = use_write / "mods"

    saves.mkdir(parents=True)

    shutil.copyfile(save_path, saves / "trainclock.zip")
    shutil.copytree(mods_path, mods)

    shutil.copyfile(script_dir / "player-data.json", use_write / "splayer-data.json")

    config_data = base_config.read_text()

    config_data = config_data.replace('read-data=__PATH__system-read-data__', 'read-data={}'.format(data_path))
    config_data = config_data.replace('write-data=__PATH__system-write-data__', 'write-data={}'.format(use_write))

    use_config.write_text(config_data)

    if include_server_config:
        server_data = base_server_settings.read_text()

        server_data = server_data.replace('"public": true,', '"public": false,')
        server_data = server_data.replace('"name": "Name of the game as it will appear in the game listing",',
                                          '"name": "clock",')
        server_data = server_data.replace('"require_user_verification": true,', '"require_user_verification": false,')
        server_data = server_data.replace('"autosave_interval": 10,', '"autosave_interval": 1000000,')

        use_server_config.write_text(server_data)


def start_server(server_folder):
    global server
    prepare_run_directory(server_folder, True)

    binary = clock_config.binary

    server = delegator.run([binary, '-c', str(server_folder / "config.ini"), '--server-settings', str(server_folder / "server-settings.json"),
                            '--start-server', str(server_folder / "write" / "saves" / "trainclock.zip")], block=False)
    print('Server started')


def start_client(client_folder):
    global client
    prepare_run_directory(client_folder, False)

    binary = clock_config.binary
    client = delegator.run([binary, '-c', str(client_folder / "config.ini"), '--mp-connect', '127.0.0.1', '--fullscreen', '--disable-audio'], block=False)
    print('Client started')


def send_time_to_server():
    server.send(generate_time_commands(dt.datetime.now()).replace("\n", "    "))


def main():
    global server
    global client

    with tempfile.TemporaryDirectory() as server_folder:
        server_folder = pathlib.Path(server_folder)
        start_server(server_folder)

        time.sleep(5)

        speed = 4
        server.send('/c game.speed = {}'.format(speed))
        server.send('/c game.surfaces["nauvis"].wind_speed = game.surfaces["nauvis"].wind_speed / {}'.format(speed))

        with tempfile.TemporaryDirectory() as client_folder:
            client_folder = pathlib.Path(client_folder)
            start_client(client_folder)

            while 1:
                time.sleep(1)
                send_time_to_server()

            # print(generate_time_commands(dt.datetime.now()))

if __name__ == "__main__":
    main()
